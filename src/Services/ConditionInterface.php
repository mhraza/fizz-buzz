<?php
declare(strict_types = 1);

namespace App\Services;

/**
 * Interface ConditionInterface
 *
 * @package App\Services
 */
interface ConditionInterface
{

    /**
     * @param int $number
     *
     * @return bool
     */
    public function isConditionMatches(int $number) :bool;


    /**
     * @return string
     */
    public function getLiteral() :string;

}