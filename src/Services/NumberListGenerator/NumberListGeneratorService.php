<?php
declare(strict_types=1);

namespace App\Services\NumberListGenerator;


use App\Services\ConditionInterface;

/**
 * Class NumberListGeneratorService
 *
 * @package App\Services\NumberListGenerator
 */
class NumberListGeneratorService implements NumberListGeneratorServiceInterface
{
    /**
     * NumberListGeneratorService constructor.
     *
     * @param ConditionInterface[] $conditions
     */
    public function __construct(
        private array $conditions
    )
    {
    }


    /**
     * Generate list
     *
     * @param int $upperBound
     *
     * @return array
     */
    public function generateList(int $upperBound = 100): array
    {
        $list = [];

        for ($number = 1; $number <= $upperBound; $number++) {

            $list[] = $this->generateElement($number);

        }

        return $list;
    }


    /**
     * @param int $number
     *
     * @return string
     */
    private function generateElement(int $number): string
    {
        foreach ($this->conditions as $condition) {

            if ($condition->isConditionMatches($number)) {

                return $condition->getLiteral();

            }

        }

        return (string) $number;
    }
}