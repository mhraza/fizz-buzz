<?php

declare(strict_types = 1);

namespace App\Services\NumberListGenerator;

/**
 * Interface NumberListGeneratorServiceInterface
 *
 * @package App\Services\NumberListGenerator
 */
interface NumberListGeneratorServiceInterface
{
    /**
     * @param int $upperBound
     *
     * @return array
     */
    public function generateList(int $upperBound = 100) :array;

}