<?php

declare(strict_types=1);

namespace App\Services\Buzz;


use App\Services\ConditionInterface;

/**
 * Class BuzzService
 *
 * @package App\Services\Buzz
 */
class BuzzService implements ConditionInterface
{

    const BUZZ_LITERAL = 'Buzz';


    /**
     * Return true if buzz condition matches
     *
     * @param int $number
     *
     * @return bool
     */
    public function isConditionMatches(int $number): bool
    {
        return $number % 5 === 0;
    }


    /**
     * Return buzz literal
     *
     * @param int $number
     *
     * @return string
     */
    public function getLiteral(): string
    {
        return self::BUZZ_LITERAL;
    }
}