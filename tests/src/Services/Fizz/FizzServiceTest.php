<?php
declare(strict_types=1);


namespace App\Tests\src\Services\Fizz;


use App\Services\Fizz\FizzService;
use PHPUnit\Framework\TestCase;

/**
 * Class FizzServiceTest
 *
 * @package App\Tests\src\Services\Fizz
 */
class FizzServiceTest extends TestCase
{
    private FizzService $fizzService;


    public function setUp(): void
    {
        parent::setUp();
        $this->fizzService = new FizzService();
    }


    public function testIsConditionMatchesYes(): void
    {
        static::assertTrue($this->fizzService->isConditionMatches(6));
    }


    public function testIsConditionMatchesNo(): void
    {
        static::assertFalse($this->fizzService->isConditionMatches(7));
    }


    public function testGetLiteral(): void
    {
        static::assertEquals('Fizz', $this->fizzService->getLiteral());
    }

}