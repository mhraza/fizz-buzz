<?php
declare(strict_types=1);


namespace App\Tests\src\Services\FizzBuzz;


use App\Services\FizzBuzz\FizzBuzzService;
use PHPUnit\Framework\TestCase;

/**
 * Class FizzBuzzServiceTest
 *
 * @package App\Tests\src\Services\FizzBuzz
 */
class FizzBuzzServiceTest extends TestCase
{
    private FizzBuzzService $fizzBuzzService;


    public function setUp(): void
    {
        parent::setUp();
        $this->fizzBuzzService = new FizzBuzzService();
    }


    public function testIsConditionMatchesYes(): void
    {
        static::assertTrue($this->fizzBuzzService->isConditionMatches(15));
    }


    public function testIsConditionMatchesNo(): void
    {
        static::assertFalse($this->fizzBuzzService->isConditionMatches(7));
    }


    public function testGetLiteral(): void
    {
        static::assertEquals('FizzBuzz', $this->fizzBuzzService->getLiteral());
    }

}