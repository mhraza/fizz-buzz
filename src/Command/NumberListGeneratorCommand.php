<?php
declare(strict_types=1);

namespace App\Command;

use App\Services\NumberListGenerator\NumberListGeneratorServiceInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'NumberListGenerator',
    description: 'Generate number list with given upper bound',
)]
class NumberListGeneratorCommand extends Command
{

    protected static $defaultName = 'app:generate-list';


    public function __construct(
        private NumberListGeneratorServiceInterface $numberListGeneratorService,
        string $name = null
    )
    {
        parent::__construct($name);
    }


    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument('upperBound', InputArgument::REQUIRED, 'Enter list upper limit')
            ->addOption(
                'upperBoundOption',
                null,
                InputOption::VALUE_NONE,
                'Enter the number until you want to generate the list i.e 100, 500'
            );
    }


    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io         = new SymfonyStyle($input, $output);
        $upperBound = $input->getArgument('upperBound');

        if (!is_numeric($upperBound)) {

            $io->error('Only numeric values are allowed for upper bound parameter!');

            return Command::FAILURE;
        }

        if ($upperBound) {

            $io->note(sprintf('You passed upper bound: %s', $upperBound));

        }

        $list = $this->numberListGeneratorService->generateList((int) $upperBound);

        foreach ($list as $item) {

            $output->writeln($item);

        }


        $io->success('Please review generated list!');

        return Command::SUCCESS;
    }
}
