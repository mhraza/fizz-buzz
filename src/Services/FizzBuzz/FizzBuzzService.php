<?php
declare(strict_types=1);

namespace App\Services\FizzBuzz;

use App\Services\ConditionInterface;

/**
 * Class FizzBuzzService
 *
 * @package App\Services\FizzBuzz
 */
class FizzBuzzService implements ConditionInterface
{

    const FIZZ_BUZZ_LITERAL = 'FizzBuzz';

    public function isConditionMatches(int $number): bool
    {
        return ($number % 3 === 0 && $number % 5 === 0);
    }


    public function getLiteral(): string
    {
        return self::FIZZ_BUZZ_LITERAL;
    }
}